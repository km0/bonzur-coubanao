var name = prompt("Comment tu t'appeles?");

  var HOST = location.origin.replace(/^http/, 'ws');
  sock = new ReconnectingWebSocket(HOST);
  var log = document.getElementById('log');
  var transUp = document.getElementById('trans');
  var trans = document.getElementById('translate');
  var textArea = document.getElementById('text');

  sock.onopen = function() {
    sock.send(JSON.stringify({
      type: "name",
      data: name
    }));
  }

  sock.onmessage = function(event) {
    var json = JSON.parse(event.data);
    if(json.type == "message"){
      chatMsg(json.name, json.data);
    }
    else if(json.type == "frase"){
    trans.innerHTML = json.data;
    }
  }

  function chatMsg(name,data){
    var msg = document.createElement('DIV');
    log.appendChild(msg);

    var author = document.createElement('DIV');
    var message = document.createElement('DIV');
    author.setAttribute('class','author');
    message.setAttribute('class','message');
    
    message.innerHTML = data;
    author.innerHTML = name;
    
    msg.appendChild(author);
    msg.appendChild(message);

    log.scrollTop = msg.offsetHeight + msg.offsetTop;
}

  
  textArea.addEventListener("keyup", function(event) {
    if (event.keyCode === 13) {
      event.preventDefault();
      document.getElementById("envoyer").click();
    }
  });


  document.querySelector('button').onclick = function() {
    var text = textArea.value;
    if(textArea.value){
    sock.send(JSON.stringify({
      type: "message",
      data: text
    }));
    chatMsg('You', text);
    textArea.value = '';
    }
  };

  document.querySelector('textarea').onclick = function() {
    transUp.scrollHeight;
    console.log('cliso');
  };