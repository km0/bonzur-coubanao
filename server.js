const express = require('express');
const PORT = process.env.PORT || 3000;
const INDEX = '/index.html';

var frase = "Questa è una frase default";

const server = express()
  .use(express.static('public'))
  .use((req,res) => res.sendFile(INDEX, { root: __dirname }))
  .listen(PORT, () => console.log(`Listening on ${PORT}`));

  
  const { Server } = require('ws');


  const s = new Server({ server });

  s.on('connection', (ws) => {
    console.log('Client connected');

    ws.on('message', function(message){
      message = JSON.parse(message);

      if(message.type == "name"){
        ws.personName = message.data;
        return;
      };

      if(message.type == "frase"){
        frase = message.data;
        ws.fraseRicevuta = message.data;
        s.clients.forEach(function e(client){
          client.send(JSON.stringify({
            type: "frase",
            data: message.data
          }));
        })
        return;
      };

      console.log("Received: " + message);
      s.clients.forEach(function e(client){
        if(client != ws)
          client.send(JSON.stringify({
            type: "message",
            name: ws.personName,
            data: message.data
          }));
      });
    });

    ws.send(JSON.stringify({
      type: "frase",
      data: frase
      }));

    ws.on('close', () => {
      console.log('Client disconnected')
    });
  });

